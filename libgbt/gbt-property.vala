/* gbt-interface.vala
 *
 * Copyright (c) 2021 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Gbt.Property : Gbt.Base, GXml.MappeableElement {
    [Description(nick="::name")]
    public string name { get; set; }
    [Description(nick="::translatable")]
    public string translatable { get; set; }
    
    /**
     * Property's value
     */
    public string @value {
        set {
            foreach (GXml.DomNode node in child_nodes)  {
                if (node is GXml.DomChildNode) {
                    ((GXml.DomChildNode) node).remove ();
                }
            }
            try {
                var t = owner_document.create_text_node (value);
                append_child (t);
            } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
            }
        }
        
        owned get {
            return text_content;
        }
    }
    
    construct {
        initialize ("property");
    }
    
    // GXml.MappeableElement
    public string get_map_key () { return name; }
    
    
    public class Map : GXml.HashMap {
        construct {
            try {
                initialize (typeof (Gbt.Property));
            } catch (GLib.Error e) { warning ("Error: "+e.message); }
        }
    }
}
