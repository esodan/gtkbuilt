vapidir = join_paths (get_option('datadir'),'vala','vapi')
GIR_NAME= VERSIONED_CAMEL_CASE_NAME+'.gir'
TYPELIB_NAME= VERSIONED_CAMEL_CASE_NAME+'.typelib'
VAPI_NAME = VERSIONED_PROJECT_NAME+'.vapi'

conf = configuration_data()
conf.set('prefix', get_option('prefix'))
conf.set('exec_prefix', get_option('prefix'))
conf.set('libdir', join_paths (get_option ('prefix'),get_option ('libdir')))
conf.set('includedir', join_paths (get_option ('includedir'), VERSIONED_PROJECT_NAME))
conf.set('PROJECT_NAME', PROJECT_NAME)
conf.set('PROJECT_VERSION', PROJECT_VERSION)
conf.set('API_VERSION', API_VERSION)
conf.set('CAMEL_CASE_NAME', CAMEL_CASE_NAME)

libgbt_pc = configure_file(input : 'gbt.pc.in',
	output : 'gbt-@0@.pc'.format(API_VERSION),
	configuration : conf
	)
install_data (libgbt_pc, install_dir : join_paths(get_option('libdir'), 'pkgconfig'))

configure_file(input : 'gbt.deps.in',
	output : 'gbt-@0@.deps'.format(API_VERSION),
	configuration : conf,
	install : true,
	install_dir : vapidir)

nsinfo = configure_file(input : 'namespace-info.vala.in',
	output : 'namespace-info.vala',
	configuration : conf)
namespaceinfo_dep = declare_dependency (sources : nsinfo)

gtkbuilt_sources = [
  'gbt-accessibility.vala',
  'gbt-class.vala',
  'gbt-base.vala',
  'gbt-child.vala',
  'gbt-interface.vala',
  'gbt-layout.vala',
  'gbt-object.vala',
  'gbt-property.vala',
  'gbt-relation.vala',
  'gbt-signal.vala',
  'gbt-style.vala',
  'gbt-template.vala',
]


version_split = meson.project_version().split('.')
MAJOR_VERSION = version_split[0]
MINOR_VERSION = version_split[1]
MICRO_VERSION = version_split[2]

version_conf = configuration_data()
version_conf.set('VERSION', meson.project_version())
version_conf.set('MAJOR_VERSION', MAJOR_VERSION)
version_conf.set('MINOR_VERSION', MINOR_VERSION)
version_conf.set('MICRO_VERSION', MICRO_VERSION)

configure_file(
  output: 'gtkbuilt-version.h',
  configuration: version_conf,
  install: true,
  install_dir: join_paths(get_option('includedir'), 'gtkbuilt')
)

gir_dirs = []
vapi_dirs = []

libgbt_deps = [
  dependency('gxml-0.20', version: '>= 0.20'),
]

libgbt_deps += namespaceinfo_dep

libgbt_args = [
		'--vapidir', meson.current_build_dir(),
		'--target-glib=2.58',
		]

libgbt = shared_library('gbt-' + API_VERSION,
  gtkbuilt_sources,
  vala_header : PROJECT_NAME+'.h',
  vala_vapi : VAPI_NAME,
  vala_gir : GIR_NAME,
  vala_args: libgbt_args,
  dependencies: libgbt_deps,
  install: true,
)

inc_libh = include_directories ('.')
inc_libh_dep = declare_dependency (include_directories : inc_libh)

libgbt_build_dir = meson.current_build_dir ()
libgbt_src_dir = meson.current_source_dir ()
libgbt_dep = declare_dependency(include_directories : inc_libh,
  link_with : libgbt,
  dependencies: libgbt_deps,
  )

g_ir_compiler = find_program('g-ir-compiler')
if get_option('introspection')
if g_ir_compiler.found()
custom_target('libgbt-typelib',
	command: [
		g_ir_compiler,
		'--shared-library', 'lib'+PROJECT_NAME+'-@0@.so'.format (API_VERSION),
		gir_dirs,
		'--output', '@OUTPUT@',
		join_paths(meson.current_build_dir(), GIR_NAME)
	],
	output: TYPELIB_NAME,
	depends: libgbt,
	install: true,
	install_dir: join_paths(get_option('libdir'), 'girepository-1.0'))
endif
endif
