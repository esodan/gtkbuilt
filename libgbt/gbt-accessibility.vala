/* gbt-accessibility.vala
 *
 * Copyright (c) 2021 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Gbt.Accessibility : Gbt.Base {
    
    Relation.Map _relations;
    public Relation.Map relations {
        get {
            if (_relations == null) {
                set_instance_property ("relations");
            }
            return _relations;
        }
        set {
            if (_relations != null) {
                try {
                    clean_property_elements ("relations");
                } catch (GLib.Error e) { warning ("Error: "+e.message); }
            }
            _relations = value;
        }
    }
    
    Property.Map _properties;
    public Property.Map properties {
        get {
            if (_properties == null) {
                set_instance_property ("properties");
            }
            return _properties;
        }
        set {
            if (_properties != null) {
                try {
                    clean_property_elements ("properties");
                } catch (GLib.Error e) { warning ("Error: "+e.message); }
            }
            _properties = value;
        }
    }
    
    construct {
        initialize ("object");
    }
    
    // GXml.MappeableElement
    public string get_map_key () { return id; }
}
