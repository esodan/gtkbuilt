/* gbt-style.vala
 *
 * Copyright (c) 2021 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Gbt.Style : GXml.Element {
    Class.Map _classes;
    public Class.Map classes {
        get {
            if (_classes == null) {
                set_instance_property ("classes");
            }
            return _classes;
        }
        set {
            if (_classes != null) {
                try {
                    clean_property_elements ("classes");
                } catch (GLib.Error e) { warning ("Error: "+e.message); }
            }
            _classes = value;
        }
    }
    construct {
        initialize ("style");
    }
}
