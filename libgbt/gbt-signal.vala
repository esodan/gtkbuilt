/* gbt-interface.vala
 *
 * Copyright (c) 2021 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Gbt.Signal : Gbt.Base,GXml.MappeableElement {
    [Description(nick="::name")]
    public string name { get; set; }
    [Description(nick="::handler")]
    public string handler { get; set; }
    /**
     * An object's type's name.
     * 
     * Use {@link assign_object} in order to the
     * correct value.
     */
    [Description(nick="::object")]
    public string object { get; set; }
    [Description(nick="::swapped")]
    public string swapped { get; set; }
    
    public void assign_object (GLib.Object obj) {
        object = obj.get_type ().name ();
    }
    
    construct {
        initialize ("signal");
    }
    
    // GXml.MappeableElement
    public string get_map_key () { return id; }
    
    public class Map : GXml.HashMap {
        construct {
            try {
                initialize (typeof (Gbt.Signal));
            } catch (GLib.Error e) { warning ("Error: "+e.message); }
        }
    }
}
